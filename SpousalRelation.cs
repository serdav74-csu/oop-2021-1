﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab1
{
    /// <summary>
    /// <c>SpousalRelation</c> представляет отношения супружества и является симметричным (<c>target</c> и <c>source</c> не имеют значения).
    /// </summary>
    class SpousalRelation : Relation
    {
        public SpousalRelation(Person target, Person source) : base(target, source)
        {

        }

        public bool Contains(Person person)
        {
            return (person == Target) || (person == Source);
        }

        public Person GetOtherSpouse(Person person)
        {
            if (!Contains(person)) { throw new ArgumentException("The given person is not a member of this relationship."); }
            return (person != Target) ? Target : Source;
        }
    }
}
