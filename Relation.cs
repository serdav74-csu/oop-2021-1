﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab1
{
    /// <summary>
    /// <c>Relation</c> - абстрактный класс, представляющий любое отношение между двумя объектами <c>Person</c>.
    /// </summary>
    abstract class Relation
    {
        public Relation(Person target, Person source)
        {
            if (target == source)
            {
                throw new ArgumentException("Relation cannot use same person for both roles!");
            }
            if (target is null || source is null)
            {
                throw new ArgumentNullException(target is null ? "target" : "source");
            }
            Target = target;
            Source = source;
        }
        public Person Target { get; init; }
        public Person Source { get; init; }
    }
}
