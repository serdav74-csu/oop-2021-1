﻿using System;
using System.Collections.Generic;
using s9_oop_lab1.validators;

namespace s9_oop_lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            var grandmother = new Person("Ефрасинья");
            var grandfather = new Person("Иннокентий");

            var mother_a = new Person("Анна");
            var father_a = new Person("Алексей");

            var mother_b = new Person("Борислава");
            var father_b = new Person("Борис");

            var son_a = new Person("Владимир");
            var daughter_a = new Person("Виктория");

            var son_b = new Person("Даниил");
            var daughter_b = new Person("Дарья");

            var relations = new Relation[]
            {
                new SpousalRelation(grandfather, grandmother),

                new ParentshipRelation(grandfather, mother_a),
                new ParentshipRelation(grandmother, mother_a),

                new ParentshipRelation(grandfather, father_b),
                new ParentshipRelation(grandmother, father_b),

                new SpousalRelation(father_a, mother_a),
                new ParentshipRelation(mother_a, son_a),
                new ParentshipRelation(father_a, son_a),
                new ParentshipRelation(mother_a, daughter_a),
                new ParentshipRelation(father_a, daughter_a),

                new SpousalRelation(father_b, mother_b),
                new ParentshipRelation(mother_b, son_b),
                new ParentshipRelation(father_b, son_b),
                new ParentshipRelation(mother_b, daughter_b),
                new ParentshipRelation(father_b, daughter_b),
            };

            var validators = new IFamilyTreeValidator[]
            {
                new OneSpouseValidator(),
                new YesManValidator(),
                new ParentCountValidator(),
                new NoCyclesValidator(),
            };

            var tree = new FamilyTree(relations, validators);

            Console.WriteLine("Дерево #1 создано успешно.");

            ShowPeople(string.Format("Родители {0}:", grandfather), tree.GetParents(grandfather));
            ShowPeople(string.Format("Родители {0}:", son_a), tree.GetParents(son_a));
            ShowPeople(string.Format("Родители {0}:", son_b), tree.GetParents(son_b));

            ShowPeople(string.Format("Братья и сёстры {0}:", daughter_a), tree.GetSiblings(daughter_a));
            ShowPeople(string.Format("Братья и сёстры {0}:", son_b), tree.GetSiblings(son_b));

            ShowPeople(string.Format("Дети {0}:", father_a), tree.GetChildren(father_a));
            ShowPeople(string.Format("Дети {0}:", mother_a), tree.GetChildren(mother_a));
            ShowPeople(string.Format("Дети {0}:", father_b), tree.GetChildren(father_b));
            ShowPeople(string.Format("Дети {0}:", grandfather), tree.GetChildren(grandfather));

            ShowPeople(string.Format("Дяди и тёти {0}:", daughter_a), tree.GetUnclesAndAunts(daughter_a));
            ShowPeople(string.Format("Двоюродные братья и сёстры {0}:", daughter_a), tree.GetCousins(daughter_a));
            ShowPeople(string.Format("Двоюродные братья и сёстры {0}:", daughter_b), tree.GetCousins(daughter_b));

            ShowPeople(string.Format("In-laws {0}:", father_a), tree.GetInLaws(father_a));
            ShowPeople(string.Format("In-laws {0}:", mother_b), tree.GetInLaws(mother_b));


            // проверим работу валидаторов

            // скопируем набор отношений, добавив новое отношение супружества (так у father_a и mother_b будет больше одного супруга)
            var invalid_tree_relations = new List<Relation>(relations);
            invalid_tree_relations.Add(new SpousalRelation(father_a, mother_b));

            try
            {
                var invalid_tree = new FamilyTree(
                    invalid_tree_relations,
                    validators
                );
            } catch (ValidationFailedException e)
            {
                Console.WriteLine("Не получилось создать дерево #2: {0}", e.Message);
            }

            // теперь сделаем то же самое, но без валидатора, выбросившего исключение
            var lax_validators = new List<IFamilyTreeValidator>();
            lax_validators.Add(new YesManValidator());

            var valid_invalid_tree = new FamilyTree(
                invalid_tree_relations,
                lax_validators
            );

            Console.WriteLine("Дерево #3 создано успешно.");

            // добавим цикл в дерево: пусть daughter_b будет родителем grandfather
            var invalid_tree_relations_with_cycle = new List<Relation>(relations);
            invalid_tree_relations_with_cycle.Add(new ParentshipRelation(daughter_b, grandfather));

            try
            {
                var invalid_tree_with_cycle = new FamilyTree(
                    invalid_tree_relations_with_cycle,
                    validators
                );
            } catch (ValidationFailedException e)
            {
                Console.WriteLine("Не получилось создать дерево #4: {0}", e.Message);
            }

            // добавим в дерево третьего родителя для daughter_b
            var invalid_tree_relations_with_extra_parent = new List<Relation>(relations);
            invalid_tree_relations_with_extra_parent.Add(new ParentshipRelation(new Person("Третий родитель"), daughter_b));

            try
            {
                var invalid_tree_with_extra_parent = new FamilyTree(
                    invalid_tree_relations_with_extra_parent,
                    validators
                );
            }
            catch (ValidationFailedException e)
            {
                Console.WriteLine("Не получилось создать дерево #5: {0}", e.Message);
            }
        }

        static void ShowPeople(string caption, IEnumerable<Person> people)
        {
            Console.WriteLine(caption);

            foreach (var person in people)
            {
                Console.WriteLine("  - {0}", person);
            }
            Console.WriteLine();
        }
    }
}
