﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab1.validators
{
    class OneSpouseValidator : IFamilyTreeValidator
    {
        public void Validate(FamilyTree tree)
        {
            IEnumerable<Relation> relations = tree.GetRelations();

            var marriedPeople = new HashSet<Person>();

            foreach (var relation in relations)
            {
                if (relation is SpousalRelation)
                {
                    Person offender;
                    if (marriedPeople.Contains(offender = relation.Source) || marriedPeople.Contains(offender = relation.Target))
                    {
                        throw new ValidationFailedException(string.Format("Person {0} has multiple spouses!", offender));
                    }
                    else
                    {
                        marriedPeople.Add(relation.Source);
                        marriedPeople.Add(relation.Target);
                    }
                }
            }
        }
    }
}
