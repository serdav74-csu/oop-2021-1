﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab1.validators
{
    // https://docs.microsoft.com/en-us/dotnet/standard/exceptions/how-to-create-user-defined-exceptions
    class ValidationFailedException : Exception
    {
        public ValidationFailedException()
        {
        }

        public ValidationFailedException(string message)
            : base(message)
        {
        }

        public ValidationFailedException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
