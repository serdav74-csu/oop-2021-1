﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab1.validators
{
    /// <summary>
    /// Интерфейс, обобщающий валидаторы для FamilyTree.
    /// </summary>
    interface IFamilyTreeValidator
    {
        /// <summary>
        /// Метод, принимающий объект FamilyTree и вызывающий исключение, если это дерево не валидно.
        /// </summary>
        /// <param name="tree">Объект FamilyTree, который необходимо валидировать.</param>
        public void Validate(FamilyTree tree);
    }
}
