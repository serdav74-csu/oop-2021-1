﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab1.validators
{
    class NoCyclesValidator : IFamilyTreeValidator
    {
        public void Validate(FamilyTree tree)
        {
            // Основная идея в том, чтобы для каждого человека пройтись по его потомкам/предкам поиском в глубину.
            // Как только текущий человек и потомок/предок совпадают, валидация завершается провалом.

            IEnumerable<Relation> relations = tree.GetRelations();

            if (!relations.Any()) return;

            var adjacency_list = tree.GetParentshipAdjacencyList();
            var visited_nodes = new HashSet<Person>();

            bool DFSCycleCheck(Person current_node, Person starting_node)
            {
                if (visited_nodes.Contains(current_node)) return false;
                visited_nodes.Add(current_node);

                foreach (var node in adjacency_list[current_node])
                {
                    if (node == starting_node) return true;
                    if (DFSCycleCheck(node, starting_node)) return true;
                }
                return false;
            }

            Person starting_node;

            while (visited_nodes.Count < adjacency_list.Count)
            {
                starting_node = (adjacency_list.Keys.Except(visited_nodes).First());

                if (DFSCycleCheck(starting_node, starting_node))
                    throw new ValidationFailedException("This tree has a person who is its own ancestor!");
            }
        }
    }


}
