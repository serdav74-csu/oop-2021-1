﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab1.validators
{
    class ParentCountValidator : IFamilyTreeValidator
    {
        public int MaxParents { get; init; }

        public ParentCountValidator(int max_parents = 2)
        {
            MaxParents = max_parents;
        }

        public void Validate(FamilyTree tree)
        {
            IEnumerable<Relation> relations = tree.GetRelations();

            var parents = GroupByChild(relations);

            KeyValuePair<Person, HashSet<Person>> offender_pair;

            if (parents.Any(IsInvalid))
            {
                offender_pair = parents.First(IsInvalid);
                throw new ValidationFailedException(string.Format("Person {0} has {1} parents, but only {2} allowed!", offender_pair.Key, offender_pair.Value.Count, MaxParents));
            }
        }

        protected bool IsInvalid(KeyValuePair<Person, HashSet<Person>> pair) => pair.Value.Count() > MaxParents;

        protected Dictionary<Person, HashSet<Person>> GroupByChild(IEnumerable<Relation> relations)
        {
            var parents = new Dictionary<Person, HashSet<Person>>(capacity: relations.Count() * 2);

            foreach (var relation in relations)
            {
                if (relation is ParentshipRelation)
                {
                    ParentshipRelation parentship_relation = relation as ParentshipRelation;
                    Person parent = parentship_relation.Parent;
                    Person child = parentship_relation.Child;

                    if (!parents.ContainsKey(child))
                    {
                        parents[child] = new HashSet<Person>();
                    }
                    parents[child].Add(parent);
                }
            }

            return parents;
        }
    }
}
