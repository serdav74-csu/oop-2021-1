﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab1
{
    /// <summary>
    /// <c>ParentshipRelation</c> представляет отношение родительства и направлено от отца/матери к сыну/дочери.
    /// </summary>
    class ParentshipRelation : Relation
    {
        public ParentshipRelation(Person parent, Person child) : base(child, parent)
        {

        }

        public Person Parent
        {
            get => Source;
        }

        public Person Child
        {
            get => Target;
        }
    }
}
