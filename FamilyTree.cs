﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using s9_oop_lab1.validators;

namespace s9_oop_lab1
{
    /// <summary>
    /// <c>FamilyTree</c> представляет генеалогическое древо на основе набора отношений <c>Relation</c>.
    /// </summary>
    class FamilyTree
    {
        public FamilyTree(IEnumerable<Relation> relations, IEnumerable<IFamilyTreeValidator> validators) {
            Relations = new List<Relation>(relations);
            Validators = new List<IFamilyTreeValidator>(validators);
            Validate();
        }

        protected List<Relation> Relations { get; init; }
        protected List<IFamilyTreeValidator> Validators { get; init; }

        /// <summary>
        /// Проверяет свой набор отношений на валидность. Выбрасывает исключение, если набор не валиден.
        /// </summary>
        protected void Validate()
        {            
            foreach (var validator in Validators)
            {
                validator.Validate(this);
            }
        }

        /// <summary>
        /// Возвращает список отношений, из которых построено дерево.
        /// </summary>
        /// <returns>Список отношений</returns>
        public ImmutableList<Relation> GetRelations()
        {
            return ImmutableList<Relation>.Empty.AddRange(Relations);
        }

        /// <summary>
        /// Возвращает список валидаторов, которые использует дерево.
        /// </summary>
        /// <returns>Список валидаторов</returns>
        public ImmutableList<IFamilyTreeValidator> GetValidators()
        {
            return ImmutableList<IFamilyTreeValidator>.Empty.AddRange(Validators);
        }

        /// <summary>
        /// Возвращает список родителей указанного человека.
        /// </summary>
        public List<Person> GetParents(Person target)
        {
            return Relations.Where(
                (relation) => (relation is ParentshipRelation && (relation as ParentshipRelation).Child == target)
            ).Select(
                (relation) => (relation as ParentshipRelation).Parent
            ).ToList();
        }

        /// <summary>
        /// Возвращает список родителей всех указанных людей, без повторов.
        /// </summary>
        public List<Person> GetParents(IEnumerable<Person> people)
        {
            return people.SelectMany((person) => GetParents(person)).Distinct().ToList();
        }

        /// <summary>
        /// Возращает супруга/супругу указанного человека. Может быть <c>null</c>.
        /// </summary>
        public Person GetSpouse(Person target)
        {
            return Relations.Where(
                (relation) => (relation is SpousalRelation && (relation as SpousalRelation).Contains(target))
            ).Select(
                (relation) => (relation as SpousalRelation).GetOtherSpouse(target)
            ).FirstOrDefault();
        }

        /// <summary>
        /// Возвращает список детей указанного человека.
        /// </summary>
        public List<Person> GetChildren(Person target)
        {
            return Relations.Where(
                (relation) => (relation is ParentshipRelation && (relation as ParentshipRelation).Parent == target)
            ).Select(
                (relation) => (relation as ParentshipRelation).Child
            ).ToList();
        }

        /// <summary>
        /// Возвращает список детей всех указанных людей, без повторов.
        /// </summary>
        public List<Person> GetChildren(IEnumerable<Person> people) {
            return people.SelectMany((person) => GetChildren(person)).Distinct().ToList();
        }

        /// <summary>
        /// Возвращает список родных братьев и сестёр указанного человека (исключая его самого).
        /// </summary>
        public List<Person> GetSiblings(Person target)
        {
            var parents = GetParents(target);
            return GetChildren(parents).Except(new[] { target }).ToList();
        }

        /// <summary>
        /// Возвращает список дядюшек и тётушек указанного человека.
        /// </summary>
        public List<Person> GetUnclesAndAunts(Person target)
        {
            var parents = GetParents(target);
            var grandparents = GetParents(parents);
            return GetChildren(grandparents).Except(parents).ToList();
        }

        /// <summary>
        /// Возвращает список двоюродных братьев и сестёр указанного человека.
        /// </summary>
        public List<Person> GetCousins(Person target)
        {
            return GetChildren(GetUnclesAndAunts(target));
        }

        /// <summary>
        /// Возвращает список in-laws (свёкров, свекровей, тёщ и тестей) указанного человека.
        /// </summary>
        public List<Person> GetInLaws(Person target)
        {
            var spouse = GetSpouse(target);
            if (spouse is null) {
                return new List<Person>();
            } else {
                return GetParents(spouse);
            }
        }

        public Dictionary<Person, List<Person>> GetParentshipAdjacencyList()
        {
            var adjacency_list = new Dictionary<Person, List<Person>>();

            foreach (var relation in GetRelations().FindAll(relation => relation is ParentshipRelation).Cast<ParentshipRelation>())
            {
                if (!adjacency_list.ContainsKey(relation.Parent))
                {
                    adjacency_list[relation.Parent] = new List<Person>();
                }
                if (!adjacency_list.ContainsKey(relation.Child))
                {
                    // хотя связи направлены от родителей к детям, детей всё равно нужно добавить в словарь,
                    // потому что они, как и родители, являются вершинами графа
                    adjacency_list[relation.Child] = new List<Person>();
                }
                adjacency_list[relation.Parent].Add(relation.Child);
            }

            return adjacency_list;
        }
    }
}
