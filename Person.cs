﻿using System.Collections.Generic;

namespace s9_oop_lab1
{
    class Person
    {
        public Person(string name)
        {
            Name = name;
        }

        public string Name { get; init; }

        public override string ToString()
        {
            return Name;
        }
    }
}
